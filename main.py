import tweepy
import sys
import ConfigParser

def fetchFollowers(user):
	followersScreenName = []
	followers = api.followers(user)
	for follower in followers:
		followersScreenName.append(follower.screen_name)
	return followersScreenName
	
def compareFollowers(filename,followersList):
	newDb = []
	try:
		file = open(filename,'r')
	except IOError:
		print(filename + " ne postoji")
		exit(1)
	for line in file:
		currentUser=line.replace(" ","").strip().split(',')
		user=[]
		try:
			user.append(currentUser[0])
			user.append(currentUser[1])
		except IndexError:
			print("Baza podataka nije u skladu sa specifikacijom")
			exit(1)
		if(currentUser[1] in followersList):
			user.append('1')
		else:
			user.append('0')
		str=",".join(user)
		str= str + "\n"
		newDb.append(str)
	file.close()
	return newDb
	
def write(filename, newDb):
	file = open(filename,'w')
	for line in newDb:
		file.write(line)
	file.close()
if __name__ == '__main__':
	#zeljeni korisnik, to ce morati biti promjenjivo
	try:
		targetUser = sys.argv[1]
	except IndexError:
		print("Program mora imati paramaetar, taj parametar je user handle")
		exit(0)
		
	Config = ConfigParser.ConfigParser()
	Config.read('config.ini')

	#pristupanje apiju za twitter

	auth = tweepy.OAuthHandler(Config.get('auth','consumer_key'), Config.get('auth','consumer_secret'))
	auth.set_access_token(Config.get('auth','access_token'), Config.get('auth','access_token_secret'))
	api = tweepy.API(auth)
	try:
		api.me()
	except tweepy.error.TweepError:
		print("Auteneifikacija nije uspjela, pogledajte congfig file")
		exit(1)

	followersScreenName = fetchFollowers(targetUser)
	newDb = compareFollowers('baza.txt',followersScreenName)
	write('baza.txt', newDb)